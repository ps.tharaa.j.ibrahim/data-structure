package com.progressoft.induction.exceptions;

public class ExceptionMsgs {

    public final static String emptyQueueMsg = "the queue is empty" ;

    public final static String fullQueueMsg = "the queue is full" ;

    public final static String emptyStackMsg = "the stack is empty" ;

    public final static String fullStackMsg = "the stack is full" ;
}

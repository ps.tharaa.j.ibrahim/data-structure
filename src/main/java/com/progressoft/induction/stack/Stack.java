package com.progressoft.induction.stack;

import com.progressoft.induction.exceptions.ExceptionMsgs;

public abstract class Stack {
    protected int capacity = 0 ;

    protected int top = 0 ;

    protected String[] stack = new String[capacity] ;

    public abstract void push(String input) ;
    public abstract void pop() ;

    public int size() {
        return top;
    }

    public String peek() {

        checkIsEmpty();

        return stack[top-1];
    }

    protected boolean isFull(){
        return top >= capacity;
    }

    protected void pop(String [] newStack){

        for (int index = 0 ; index < top-1 ; index ++){
            newStack[index] = stack[index] ;
        }
        stack = newStack ;

        top-- ;
    }
    protected void checkIsEmpty(){
        if (top == 0)  throw new NegativeArraySizeException(ExceptionMsgs.emptyStackMsg) ;
    }

    public String[] getStack() {
        return stack;
    }
}

package com.progressoft.induction.stack;

public class DynamicStack extends Stack {
    @Override
    public void push(String input) {

     String [] newStack = new String[++capacity] ;
     int oldStackCapacity = stack.length ;

     if(stack.length != 0){
         for(int index = 0 ; index < oldStackCapacity ; index++){
             newStack[index] = stack[index] ;
             newStack[capacity -1] = input ;
         }
     }else newStack[0] = input ;

     top ++ ;
     stack = newStack ;
    }

    @Override
    public void pop() {

        checkIsEmpty();

        String [] newStack = new String[--capacity];

        pop(newStack) ;
    }


}

package com.progressoft.induction.stack;

import com.progressoft.induction.exceptions.ExceptionMsgs;

public class FixedStack extends Stack {


    public FixedStack(int capacity) {

        this.stack = new String[capacity];
        this.capacity = capacity ;
    }

    public void push(String input) {

        if(isFull()){
            throw new ArrayIndexOutOfBoundsException(ExceptionMsgs.fullStackMsg);
        }
        stack[top] = input ;

        top++ ;
    }

    @Override
    public void pop() {

        checkIsEmpty();

        String [] newStack = new String[capacity] ;

        pop(newStack);
    }


}

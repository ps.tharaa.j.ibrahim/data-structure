package com.progressoft.induction.queue;

import com.progressoft.induction.exceptions.ExceptionMsgs;

public abstract class Queue {
    protected int capacity = 0;

    protected int read = 0 ;

    protected final int head = 0 ;
    protected String[] queue = new String[capacity] ;

    public abstract void enque(String input) ;

    public abstract void deque() ;

    public int size() {
        return read ;
    }

    public String peek() {
         checkIsEmpty();
         return queue[head];
    }

    protected boolean isFull(){

        return read >= capacity;

    }

    protected void checkIsEmpty(){

        if (read == 0)  throw new NegativeArraySizeException(ExceptionMsgs.emptyQueueMsg) ;
    }

    protected void deque(String [] newQueue){

        int newQueueIndex = 0 ;
        for (int index = 1 ; index < read ; index ++) {
            newQueue[newQueueIndex] = queue[index] ;
            newQueueIndex ++ ;
        }
        queue = newQueue ;
        read -- ;

    }

    public String[] getQueue() {
        return queue;
    }
}

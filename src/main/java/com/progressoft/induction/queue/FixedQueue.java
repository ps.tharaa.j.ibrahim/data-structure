package com.progressoft.induction.queue;

import com.progressoft.induction.exceptions.ExceptionMsgs;

public class FixedQueue extends Queue {


    public FixedQueue(int capacity) {
        this.capacity = capacity;
        this.queue = new String [this.capacity] ;
    }

    @Override
    public void enque(String input) {

        if(isFull()) throw new ArrayIndexOutOfBoundsException(ExceptionMsgs.fullQueueMsg) ;
        queue[read] = input ;
        read++ ;
    }

    @Override
    public void deque() {
        checkIsEmpty();

        String[] newQueue = new String[capacity];

        deque(newQueue);
    }


}

import com.progressoft.induction.exceptions.ExceptionMsgs;
import com.progressoft.induction.queue.Queue;
import com.progressoft.induction.queue.DynamicQueue;
import com.progressoft.induction.queue.FixedQueue;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;

public class QueueTests {

    private Queue queue ;
    private int size;



    @Test
    void givenSizeOfTheQueue_whenEnqueValidNumberOfIndexes_thenAddElementToTheReadOfTheQueue(){

        size = 3 ;
        String [] expectedStack = {"tharaa" , "jamil" , "harb"} ;
        generateQueue("fixed");

        assertArrayEquals(expectedStack , queue.getQueue()) ;
    }

    @Test
    void givenSizeOfTheQueue_whenEnqueIndexLargerThanQueueSize_thenThrowsArrayIndexOutOfBoundsException(){

        size = 3 ;
        String expectedMsg = ExceptionMsgs.fullQueueMsg;
        generateQueue("fixed");

        ArrayIndexOutOfBoundsException exception = assertThrows(ArrayIndexOutOfBoundsException.class , () -> queue.enque("tharaa")) ;
        assertEquals(expectedMsg , exception.getMessage() );


    }

    @Test
    void givenSizeOfTheQueue_whenDeque_thenRemoveElementsFromTheHeadOfTheQueue(){

        size = 3 ;
        String [] expectedQueue = {"jamil" , "harb" , null} ;
        generateQueue("fixed");
        queue.deque();

        assertArrayEquals(expectedQueue , queue.getQueue());

    }

    @Test
    void givenFixedEmptyQueue_whenDeque_thenThrowsArrayIndexOutOfBoundsException(){

        size = 3 ;
        String expectedMsg = ExceptionMsgs.emptyQueueMsg;
        Queue emptyQueue = new FixedQueue(size) ;

        NegativeArraySizeException exception = assertThrows(NegativeArraySizeException.class , () -> emptyQueue.deque()) ;
        assertEquals(expectedMsg , exception.getMessage() );

    }

    @Test
    void givenSizeOfQueue_whenSize_thenReturnTheCurrentNumberOfElementsInTheQueue(){
       int expectedSize = 3;
       size = expectedSize ;

       generateQueue("fixed"); ;
       int actualSize = queue.size();

       assertEquals(expectedSize , actualSize);

    }

    @Test
    void givenSizeOfQueue_whenPeek_thenReturnTheElementAtTheHeadOfTheQueue(){

        size = 3 ;
        String expected = "tharaa" ;
        generateQueue("fixed");


        assertEquals(expected , queue.peek());
    }

    @Test
    void givenFixedEmptyQueue_whenPeek_thenThrowsArrayIndexOutOfBoundsException(){
        size = 3 ;
        String expectedMsg = ExceptionMsgs.emptyQueueMsg ;
        Queue emptyQueue = new FixedQueue(3) ;

        NegativeArraySizeException exception = assertThrows(NegativeArraySizeException.class , () -> emptyQueue.peek()) ;
        assertEquals(expectedMsg , exception.getMessage() );

    }

    @Test
    void givenDynamicQueue_whenEnqueValid_thenAddElementToTheReadOfTheQueue(){
        String [] expectedQueue = {"tharaa" , "jamil" , "harb"} ;

        generateQueue("dynamic");

        assertArrayEquals(expectedQueue , queue.getQueue()) ;
    }

    @Test
    void givenDynamicQueue_whenDeque_thenRemoveElementsFromTheHeadOfTheQueue(){
        String [] expectedQueue = {"jamil" , "harb"} ;
        generateQueue("dynamic");
        queue.deque();

        assertArrayEquals(expectedQueue , queue.getQueue());

    }

    @Test
    void givenDynamicEmptyQueue_whenDeque_thenThrowsArrayIndexOutOfBoundsException(){

        String expectedMsg = ExceptionMsgs.emptyQueueMsg ;
        Queue emptyQueue = new DynamicQueue() ;

        NegativeArraySizeException exception = assertThrows(NegativeArraySizeException.class , () -> emptyQueue.deque()) ;
        assertEquals(expectedMsg , exception.getMessage() );

    }
    @Test
    void givenDynamicQueue_whenSize_thenReturnTheSizeOfQueue(){
        int expectedSize = 3 ;
        generateQueue("dynamic");
        int actualSize = queue.size();

        assertEquals(expectedSize , actualSize);

    }

    @Test
    void givenDynamicQueue_whenPeek_thenReturnTheElementAtTheHeadOfTheQueue(){

        String expected = "tharaa" ;

        generateQueue("dynamic");

        assertEquals(expected , queue.peek());
    }

    @Test
    void givenDynamicEmptyQueue_whenPeek_thenThrowsArrayIndexOutOfBoundsException(){

        String expectedMsg = ExceptionMsgs.emptyQueueMsg ;
        Queue emptyQueue = new DynamicQueue() ;

        NegativeArraySizeException exception = assertThrows(NegativeArraySizeException.class , () -> emptyQueue.peek()) ;
        assertEquals(expectedMsg , exception.getMessage() );

    }

    private void generateQueue(String typeOfQueue){

        if(typeOfQueue.equals("fixed")){
            queue  = new FixedQueue(size) ;
        }else queue = new DynamicQueue() ;

        queue.enque("tharaa");
        queue.enque("jamil");
        queue.enque("harb");
    }



}

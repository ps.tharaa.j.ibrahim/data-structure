import com.progressoft.induction.exceptions.ExceptionMsgs;
import com.progressoft.induction.stack.DynamicStack;
import com.progressoft.induction.stack.FixedStack;
import com.progressoft.induction.stack.Stack;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;

public class StackTests {

    private Stack stack ;

    private int size = 0 ;


    @Test
    void givenSizeOfTheStack_whenPushValidNumberOfIndexes_thenAddElementToTheTopOfTheStack(){
        size = 3 ;
        String [] expectedStack = {"tharaa" , "jamil" , "harb"} ;
        generateStack("fixed");

        assertArrayEquals(expectedStack , stack.getStack()) ;
    }

    @Test
    void givenSizeOfTheStack_whenPushIndexLargerThanStackSize_thenThrowsArrayIndexOutOfBoundsException(){
        size = 3 ;
        String expectedMsg = ExceptionMsgs.fullStackMsg;
        generateStack("fixed");

        ArrayIndexOutOfBoundsException exception = assertThrows(ArrayIndexOutOfBoundsException.class , () -> stack.push("tharaa")) ;
        assertEquals(expectedMsg , exception.getMessage() );


    }

    @Test
    void givenSizeOfTheStack_whenPop_thenPopElementFromTheStackTop(){
        size = 3 ;
        String [] expectedStack = {"tharaa" , "jamil" , null} ;
        generateStack("fixed");
        stack.pop();

        assertArrayEquals(expectedStack , stack.getStack());

    }

    @Test
    void givenEmptyStack_whenPop_thenThrowsArrayIndexOutOfBoundsException(){
        size = 3 ;
        String expectedMsg = ExceptionMsgs.emptyStackMsg ;
        Stack emptyStack = new FixedStack(size) ;

        NegativeArraySizeException exception = assertThrows(NegativeArraySizeException.class , () -> emptyStack.pop()) ;
        assertEquals(expectedMsg , exception.getMessage() );

    }
    @Test
    void givenSizeOfStack_whenSize_thenReturnTheCurrentNumberOfElementsInTheStack(){

        int expectedSize = 3;
        size = expectedSize ;
        generateStack("fix");
        int actualSize = stack.size();

        assertEquals(expectedSize , actualSize);

    }

    @Test
    void givenSizeOfStack_whenPeek_thenReturnReturnTheElementAtTheTopOfTheStack(){

        size = 3 ;
        String expected = "harb" ;
        generateStack("fixed");


        assertEquals(expected , stack.peek());
    }

    @Test
    void givenEmptyStack_whenPeek_thenThrowsArrayIndexOutOfBoundsException(){
        size = 3 ;
        String expectedMsg =  ExceptionMsgs.emptyStackMsg ;
        Stack emptyStack = new FixedStack(size) ;

        NegativeArraySizeException exception = assertThrows(NegativeArraySizeException.class , () -> emptyStack.peek()) ;
        assertEquals(expectedMsg , exception.getMessage() );

    }

    @Test
    void givenDynamicStack_whenPush_thenAddElementToTheTopOfTheStack(){
        String [] expectedStack = {"tharaa" , "jamil" , "harb"} ;

        generateStack("dynamic");

        assertArrayEquals(expectedStack , stack.getStack()) ;
    }

    @Test
    void givenDynamicStack_whenPop_thenPopElementFromTheStackTop(){
        String [] expectedStack = {"tharaa" , "jamil"} ;

        generateStack("dynamic");
        stack.pop();

        assertArrayEquals(expectedStack , stack.getStack());

    }

    @Test
    void givenDynamicEmptyStack_whenPop_thenThrowsArrayIndexOutOfBoundsException(){

        String expectedMsg =  ExceptionMsgs.emptyStackMsg ;
        Stack emptyStack = new DynamicStack() ;

        NegativeArraySizeException exception = assertThrows(NegativeArraySizeException.class , () -> emptyStack.pop()) ;
        assertEquals(expectedMsg , exception.getMessage() );

    }

    @Test
    void givenDynamicStack_whenSize_thenReturnTheCurrentNumberOfElementsInTheStack(){
        int expectedSize = 3;
        generateStack("dynamic");
        int actualSize = stack.size();

        assertEquals(expectedSize , actualSize);

    }

    @Test
    void givenDynamicSize_whenPeek_thenReturnReturnTheElementAtTheTopOfTheStack(){

        String expected = "harb" ;
        generateStack("dynamic");

        assertEquals(expected , stack.peek());
    }

    @Test
    void givenEmptyDynamicStack_whenPeek_thenThrowsArrayIndexOutOfBoundsException(){

        String expectedMsg =  ExceptionMsgs.emptyStackMsg ;
        Stack emptyStack = new DynamicStack() ;

        NegativeArraySizeException exception = assertThrows(NegativeArraySizeException.class , () -> emptyStack.peek()) ;
        assertEquals(expectedMsg , exception.getMessage() );

    }
    private void generateStack(String stackType){

        if (stackType.equals("fixed")) stack = new FixedStack(size);
        else stack = new DynamicStack() ;

        stack.push("tharaa");
        stack.push("jamil");
        stack.push("harb");

}
}
